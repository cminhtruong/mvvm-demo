package mvvm.cmtruong.com.mvvmdemo.view;

public interface CompletedListener {
    void onCompleted();
}
