package mvvm.cmtruong.com.mvvmdemo.service;

import android.util.Log;

import java.util.List;

import mvvm.cmtruong.com.mvvmdemo.model.Movie;
import mvvm.cmtruong.com.mvvmdemo.model.Response;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class MovieRetrofitImpl {
    private static final String TAG = MovieRetrofitImpl.class.getSimpleName();
    private Retrofit retrofit;
    private MovieService service;
    private OkHttpClient.Builder builder;

    private static class Singleton {
        private static final MovieRetrofitImpl INSTANCE = new MovieRetrofitImpl();
    }

    public static MovieRetrofitImpl getInstance() {
        return Singleton.INSTANCE;
    }

    public MovieRetrofitImpl() {
        builder = new OkHttpClient.Builder();
        Log.d(TAG, "MovieRetrofitImpl: " + builder.toString());
        retrofit = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(MovieService.URL)
                .build();
        Log.d(TAG, "MovieRetrofitImpl: " + retrofit.toString());
        service = retrofit.create(MovieService.class);
    }

    public void getMovies(Subscriber<Movie> subscriber, int start, int count) {
        service.getMovies(start, count)
                .map(new Func1<Response<List<Movie>>, List<Movie>>() {
                    @Override
                    public List<Movie> call(Response<List<Movie>> listResponse) {
                        Log.d(TAG, "call: " + listResponse.toString());
                        return listResponse.getSubjects();
                    }
                })
                .flatMap(new Func1<List<Movie>, Observable<Movie>>() {
                    @Override
                    public Observable<Movie> call(List<Movie> movies) {
                        Log.d(TAG, "call: " + movies.toString());
                        return Observable.from(movies);
                }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
