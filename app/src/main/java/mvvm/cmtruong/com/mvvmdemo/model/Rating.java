package mvvm.cmtruong.com.mvvmdemo.model;

public class Rating {
    private float average;

    public void setAverage(float average) {
        this.average = average;
    }

    public float getAverage() {
        return average;
    }
}
