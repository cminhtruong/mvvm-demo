package mvvm.cmtruong.com.mvvmdemo.service;


import java.util.List;

import mvvm.cmtruong.com.mvvmdemo.model.Movie;
import mvvm.cmtruong.com.mvvmdemo.model.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface MovieService {

    String URL = "https://api.douban.com/v2/movie/";

    @GET("top250")
    Observable<Response<List<Movie>>> getMovies(@Query("start") int start, @Query("count") int count);
}
