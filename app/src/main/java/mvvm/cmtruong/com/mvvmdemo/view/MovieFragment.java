package mvvm.cmtruong.com.mvvmdemo.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mvvm.cmtruong.com.mvvmdemo.R;
import mvvm.cmtruong.com.mvvmdemo.adapter.MovieAdapter;
import mvvm.cmtruong.com.mvvmdemo.databinding.MovieFragmentBinding;
import mvvm.cmtruong.com.mvvmdemo.vm.MainViewModel;


public class MovieFragment extends Fragment implements CompletedListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = MovieFragment.class.getSimpleName();

    private MainViewModel mainViewModel;
    private MovieFragmentBinding movieFragmentBinding;
    private MovieAdapter adapter;

    public static MovieFragment getInstance() {
        return new MovieFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.movie_fragment, container, false);
        movieFragmentBinding = MovieFragmentBinding.bind(contentView);
        Log.d(TAG, "onCreateView: checked");
        initData();
        return contentView;
    }

    @Override
    public void onRefresh() {
        adapter.clearItems();
        mainViewModel.refreshData();
    }

    @Override
    public void onCompleted() {
        if (movieFragmentBinding.swipeRefreshLayout.isRefreshing()) {
            movieFragmentBinding.swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void initData() {
        adapter = new MovieAdapter();
        movieFragmentBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        movieFragmentBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        movieFragmentBinding.recyclerView.setAdapter(adapter);
        movieFragmentBinding.swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark);
        movieFragmentBinding.swipeRefreshLayout.setOnRefreshListener(this);
        mainViewModel = new MainViewModel(adapter, this);
        movieFragmentBinding.setViewModel(mainViewModel);

    }
}
