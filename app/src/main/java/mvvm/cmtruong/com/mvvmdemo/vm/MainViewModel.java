package mvvm.cmtruong.com.mvvmdemo.vm;

import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;

import mvvm.cmtruong.com.mvvmdemo.adapter.MovieAdapter;
import mvvm.cmtruong.com.mvvmdemo.model.Movie;
import mvvm.cmtruong.com.mvvmdemo.service.MovieRetrofitImpl;
import mvvm.cmtruong.com.mvvmdemo.view.CompletedListener;
import rx.Subscriber;

public class MainViewModel {
    public ObservableField<Integer> contentViewVisibility;
    public ObservableField<Integer> progressBarVisibility;
    public ObservableField<Integer> errorInfoLayoutVisibility;
    public ObservableField<String> exception;
    private Subscriber<Movie> subscriber;
    private MovieAdapter adapter;
    private CompletedListener completedListener;
    private static final String TAG = MainViewModel.class.getSimpleName();

    public MainViewModel(MovieAdapter adapter, CompletedListener completedListener) {
        this.adapter = adapter;
        this.completedListener = completedListener;
        initData();
        getMovies();
    }

    void initData() {
        contentViewVisibility = new ObservableField<>();
        progressBarVisibility = new ObservableField<>();
        errorInfoLayoutVisibility = new ObservableField<>();
        exception = new ObservableField<>();
        contentViewVisibility.set(View.GONE);
        errorInfoLayoutVisibility.set(View.GONE);
        progressBarVisibility.set(View.VISIBLE);
    }

    private void hideAll() {
        contentViewVisibility.set(View.GONE);
        errorInfoLayoutVisibility.set(View.GONE);
        progressBarVisibility.set(View.GONE);
    }

    public void refreshData() {
        getMovies();
    }


    private void getMovies() {
        subscriber = new Subscriber<Movie>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted: checked");
                hideAll();
                contentViewVisibility.set(View.VISIBLE);
                completedListener.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                hideAll();
                errorInfoLayoutVisibility.set(View.VISIBLE);
                exception.set(e.getMessage());
            }

            @Override
            public void onNext(Movie movie) {
                adapter.addItem(movie);
            }
        };
        MovieRetrofitImpl.getInstance().getMovies(subscriber, 0, 20);
    }

}
